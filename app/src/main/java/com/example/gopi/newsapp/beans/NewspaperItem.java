package com.example.gopi.newsapp.beans;

/**
 * Created by GOPI on 3/8/2017.
 */

public class NewspaperItem {
    private int newsLogo;
    private String title;

    public NewspaperItem(int newsLogo, String title) {
        this.newsLogo = newsLogo;
        this.title = title;
    }

    public int getNewsLogo() {
        return newsLogo;
    }

    public void setNewsLogo(int newsLogo) {
        this.newsLogo = newsLogo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
