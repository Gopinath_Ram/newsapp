package com.example.gopi.newsapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gopi.newsapp.beans.NewspaperItem;
import com.example.gopi.newsapp.R;
import com.example.gopi.newsapp.activity.NewsFeed;

import java.util.List;

/**
 * Created by GOPI on 3/8/2017.
 */

public class NewsTitleAdapter extends RecyclerView.Adapter<NewsTitleAdapter.ViewHolder>{
    private List<NewspaperItem> itemlist;
    private Context mContext;

    public NewsTitleAdapter(List<NewspaperItem> itemlist, Context mContext) {
        this.itemlist = itemlist;
        this.mContext = mContext;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView newspaperTitle;
        public ImageView newsPaperImage;
        public ViewHolder(final View itemView) {
            super(itemView);
            newsPaperImage = (ImageView)itemView.findViewById(R.id.news_logo);
            newspaperTitle = (TextView)itemView.findViewById(R.id.news_title);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler,null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.newspaperTitle.setText(itemlist.get(position).getTitle());
        holder.newsPaperImage.setImageResource(itemlist.get(position).getNewsLogo());
        holder.newsPaperImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(),NewsFeed.class);
                intent.putExtra("newsposition",position);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemlist.size();
    }
}
