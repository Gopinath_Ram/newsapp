package com.example.gopi.newsapp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.gopi.newsapp.R;
import com.example.gopi.newsapp.VolleyImage;
import com.example.gopi.newsapp.adapter.NewsFeedAdapter;
import com.example.gopi.newsapp.beans.Article;
import com.example.gopi.newsapp.beans.ArticleList;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;

public class NewsFeed extends AppCompatActivity {
    String url;
    String urldetail = null;
    private ListView mListview;
    private NewsFeedAdapter mNewsAdapter;
    JsonObjectRequest objectRequest;
    ProgressDialog progressDialog;
    JSONArray obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_feed);
        mListview = (ListView) findViewById(R.id.listView);
        progressDialog = new ProgressDialog(NewsFeed.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Intent intent = getIntent();
        int value = intent.getIntExtra("newsposition", 0);
        switch (value) {
            case 0:
                getSupportActionBar().setTitle("BBC News");
                url = "https://newsapi.org/v1/articles?source=bbc-news&sortBy=top&apiKey=2ef595b2741e4a579066eef4f638650a";
                break;
            case 1:
                getSupportActionBar().setTitle("Business Insider");
                url = "https://newsapi.org/v1/articles?source=business-insider&sortBy=top&apiKey=2ef595b2741e4a579066eef4f638650a";
                break;
            case 2:
                getSupportActionBar().setTitle("CNBC");
                url = "https://newsapi.org/v1/articles?source=cnbc&sortBy=top&apiKey=2ef595b2741e4a579066eef4f638650a";
                break;
            case 3:
                getSupportActionBar().setTitle("Financial Times");
                url = "https://newsapi.org/v1/articles?source=financial-times&sortBy=top&apiKey=2ef595b2741e4a579066eef4f638650a";
                break;
            case 4:
                getSupportActionBar().setTitle("CNN");
                url = "https://newsapi.org/v1/articles?source=cnn&sortBy=top&apiKey=2ef595b2741e4a579066eef4f638650a";
                break;
            case 5:
                getSupportActionBar().setTitle("ESPN");
                url = "https://newsapi.org/v1/articles?source=espn&sortBy=top&apiKey=2ef595b2741e4a579066eef4f638650a";
                break;
            default:
                break;
        }
        objectRequest = new JsonObjectRequest(url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ArticleList articleList = new Gson().fromJson(response.toString(),ArticleList.class);
                try {
                    obj = response.getJSONArray("articles");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mNewsAdapter = new NewsFeedAdapter(articleList.getArticles(),NewsFeed.this);
                mListview.setAdapter(mNewsAdapter);
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        VolleyImage.getInstance(NewsFeed.this).addToRequestQueue(objectRequest);
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    JSONObject jsonObject = obj.getJSONObject(i);
                    urldetail = jsonObject.getString("url");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent1 = new Intent(NewsFeed.this,DetailedNews.class);
                intent1.putExtra("url",urldetail);
                startActivity(intent1);
            }
        });
    }
}
