package com.example.gopi.newsapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ListView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.example.gopi.newsapp.adapter.NewsFeedAdapter;
import com.example.gopi.newsapp.adapter.NewsTitleAdapter;
import com.example.gopi.newsapp.beans.NewspaperItem;
import com.example.gopi.newsapp.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private GridLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Choose Newspaper");
        List<NewspaperItem> newspaperItems = getAllItemList();
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        mRecyclerView.setHasFixedSize(false);
        mLayoutManager = new GridLayoutManager(MainActivity.this,2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        NewsTitleAdapter mAdapter = new NewsTitleAdapter(newspaperItems,MainActivity.this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private List<NewspaperItem> getAllItemList() {
        List<NewspaperItem> newspaperItems = new ArrayList<NewspaperItem>();
        newspaperItems.add(new NewspaperItem(R.drawable.bbc,"BBC News"));
        newspaperItems.add(new NewspaperItem(R.drawable.bi,"Business Insider"));
        newspaperItems.add(new NewspaperItem(R.drawable.cnbc,"CNBC"));
        newspaperItems.add(new NewspaperItem(R.drawable.ft,"Financial Times"));
        newspaperItems.add(new NewspaperItem(R.drawable.cnn,"CNN"));
        newspaperItems.add(new NewspaperItem(R.drawable.espn,"ESPN"));
        return newspaperItems;
    }
}
