package com.example.gopi.newsapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.gopi.newsapp.R;
import com.example.gopi.newsapp.VolleyImage;
import com.example.gopi.newsapp.beans.Article;

import java.util.List;

/**
 * Created by GOPI on 3/9/2017.
 */

public class NewsFeedAdapter extends BaseAdapter {
    Context context;
    List<Article> articleList;
    LayoutInflater layoutInflater;

    ImageLoader imageLoader = VolleyImage.getInstance(context).getImageLoader();
    public NewsFeedAdapter(List<Article> articleList, Context context) {
        this.articleList = articleList;
        this.context = context;
        layoutInflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return articleList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view==null){
            view=layoutInflater.inflate(R.layout.item_listview,viewGroup,false);
        }
        if (imageLoader == null)
            imageLoader = VolleyImage.getInstance(context).getImageLoader();
        Article m = articleList.get(i);
        //TextView tvauthor=(TextView)view.findViewById(R.id.title);
        TextView tvtitle=(TextView)view.findViewById(R.id.title);
        //TextView tvdescription=(TextView)view.findViewById(R.id.tvdescription);
        TextView tvpublished=(TextView)view.findViewById(R.id.publishedid);
        NetworkImageView thumbNail1 = (NetworkImageView)view.findViewById(R.id.thumbnailview);
        thumbNail1.setImageUrl(m.getUrlToImage(), imageLoader);
        //tvauthor.setText(articleList.get(i).getAuthor());
        tvtitle.setText(articleList.get(i).getTitle());
        //tvdescription.setText(articleList.get(i).getDescription());
        tvpublished.setText(articleList.get(i).getPublishedAt());
        return view;
    }
}